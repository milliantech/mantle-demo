#include <iostream>
#include <vector>

#include "SceneObjectComponent/GRMeshRenderer.h"
#include "SceneObjectComponent/GRTransform.h"

#include "GRRenderSystem.h"
#include "GRRenderSystemResourceManager.h"

#include "GRGPUMemoryManager.h"
#include "GRRenderWorker.h"
#include "GRRenderTarget.h"
#include "GRRenderTree.h"
#include "GRMesh.h"
#include "GRBuffer.h"
#include "GRDescriptorSet.h"
#include "GRGPUMemory.h"
#include "GRMaterial.h"
#include "GRSceneObject.h"
#include "GRPipeline.h"
#include "GRScene.h"
#include "GRViewport.h"
#include "GRTexture.h"
#include "GRCommandBuffer.h"
#include "GRMemoryRefSet.h"

#include "GRUtils.h"

namespace GREngine
{

namespace SceneObjectComponents
{

struct RenderObjectConstants
{
	DirectX::XMMATRIX mMatrix;
	DirectX::XMMATRIX nMatrix;
};

class MeshRenderCommand
{
public:
	MeshRenderCommand(MeshRenderer *meshRenderer) :
		mRenderer(meshRenderer)
	{
	}

	virtual ~MeshRenderCommand()
	{
	}
	
	virtual void draw(CommandBufferSPtr &cmdBuff, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context)
	{	
		MemoryRefSet refs;

		const GR_MEMORY_VIEW_ATTACH_INFO indexiesView = mRenderer->mMesh->indexiesView(refs);
		const GR_ENUM indexSize = GR_INDEX_16;

		grCmdBindIndexData(cmdBuff->grCmdBuffer(), indexiesView.mem, indexiesView.offset, indexSize);

		grCmdDrawIndexed(cmdBuff->grCmdBuffer(), 0, mRenderer->mMesh->indexiesCount(), 0, 0, 1);
	}

private:
	MeshRenderer *mRenderer;
};

MeshRenderer::MeshRenderer(SceneObject *obj) :
	SceneObjectComponent(obj),
	mTransform(obj->component<Transform>()),
	mRenderCommand(nullptr)
{
	initGPUMemory();
}

MeshRenderer::~MeshRenderer()
{
	
}

MeshSPtr& MeshRenderer::mesh()
{
	MutexGuard locker(mGuard);

	return mMesh;
}

MaterialSPtr& MeshRenderer::material()
{
	MutexGuard locker(mGuard);

	return mMaterial;
}

void MeshRenderer::setMesh(MeshSPtr mesh)
{
	MutexGuard locker(mGuard);

	mMesh = mesh;
	mRenderCommand = nullptr;
}

void MeshRenderer::setMaterial(MaterialSPtr material)
{
	MutexGuard locker(mGuard);

	mMaterial = material;
	mRenderCommand = nullptr;
}

bool MeshRenderer::canDraw() const
{
	return mMesh != nullptr && mMaterial != nullptr;
}

void MeshRenderer::draw(CommandBufferSPtr &cmdBuff, const RenderPhaseParamsSPtr &renderParams, const RenderContext & context)
{
	MutexGuard locker(mGuard);

	if (!canDraw())
		return;

	if (mRenderCommand == nullptr)
		mRenderCommand = std::make_shared<MeshRenderCommand>(this);

	mRenderCommand->draw(cmdBuff, renderParams, context);
}

void MeshRenderer::bindData(MemoryRefSet &memoryRefs, const DescriptorSetSPtr & descriptorSet, GR_UINT descriptorOffset, const RenderPhaseParamsSPtr & renderParams, const RenderContext & context)
{
	mMesh->indexiesView(memoryRefs);

	mMaterial->bindData(memoryRefs, descriptorSet, descriptorOffset, renderParams, context);
}

void MeshRenderer::initGPUMemory()
{
	mConstantsBuffer = GPUMemoryManager::getInstance().allocateBufferFromGPUPool(sizeof(RenderObjectConstants));
	
	//GPUMemorySPtr constantsMemory = GPUMemoryManager::getInstance().allocateGPUMemory(sizeof(RenderObjectConstants));
	//mConstantsBuffer = std::make_shared<Buffer>(constantsMemory, sizeof(RenderObjectConstants));
}

void MeshRenderer::updateGPUMemory(CommandBufferSPtr &cmdBuff, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context)
{
	DirectX::XMMATRIX transformMatrix = mTransform->matrix();// DirectX::XMMatrixMultiply(context.parentTransform, mTransform->matrix());

	DirectX::XMMATRIX normalMatrix = DirectX::XMMatrixRotationQuaternion(mTransform->rotation());

	RenderObjectConstants constants;
	constants.mMatrix = DirectX::XMMatrixTranspose(transformMatrix);
	constants.nMatrix = DirectX::XMMatrixTranspose(normalMatrix);

	mConstantsBuffer->update(cmdBuff, sizeof(constants), &constants);
	mConstantsBuffer->setState(cmdBuff, GR_MEMORY_STATE_GRAPHICS_SHADER_READ_ONLY);

	mMaterial->updateGPUMemory(cmdBuff, renderParams, context);
}

GR_MEMORY_VIEW_ATTACH_INFO MeshRenderer::objectConstantsView(MemoryRefSet &refs) const
{
	refs.insert(mConstantsBuffer->grReference());

	return mConstantsBuffer->grView({GR_CH_FMT_UNDEFINED, GR_NUM_FMT_UNDEFINED}, sizeof(RenderObjectConstants));
}

GR_MEMORY_VIEW_ATTACH_INFO MeshRenderer::meshConstantsView(MemoryRefSet &refs) const
{
	return mMesh->meshView(refs);
}

}

}
