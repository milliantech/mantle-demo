#include <iostream>

#include "GRSceneManager.h"
#include "GRRenderSystem.h"
#include "GRLog.h"
#include "GRScene.h"

namespace GREngine
{ 

SceneManager::SceneManager()
{
	mRenderSystem = RenderSystem::getInstancePtr();

	init();

	Log::info("SceneManager created");
}


SceneManager::~SceneManager()
{
	mRenderSystem = 0;
	
	shutdown();

	Log::info("SceneManager destroyed");
}

void SceneManager::init()
{
	mScenes.clear();
}

void SceneManager::shutdown()
{
	MutexGuard locker(mGuard);

	for (const auto &pair : mScenes)
	{
		pair.second->setActive(false);
	}

	mScenes.clear();
}

SceneWPtr SceneManager::createScene(const std::string &id)
{
	SceneSPtr scenePtr = std::make_shared<Scene>(id);

	MutexGuard locker(mGuard);

	mScenes[id] = scenePtr;

	return scenePtr;
}

SceneWPtr SceneManager::getScene(const std::string &id) const
{
	MutexGuard locker(mGuard);

	return mScenes.at(id);
}

void SceneManager::removeScene(SceneWPtr scene)
{
	MutexGuard locker(mGuard);

	if(SceneSPtr ptr = scene.lock())
	{
		mScenes.erase(ptr->getId());
	}
}

}