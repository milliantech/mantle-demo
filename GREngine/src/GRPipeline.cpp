#include "GRPipeline.h"
#include "GRRenderSystem.h"
#include "GRGPUMemoryManager.h"

namespace GREngine
{

Pipeline::Pipeline(const GR_GRAPHICS_PIPELINE_CREATE_INFO &info)
{
	RenderSystem *rs = RenderSystem::getInstancePtr();
	GPUMemoryManager *mm = GPUMemoryManager::getInstancePtr();

	grCreateGraphicsPipeline(rs->device(), &info, &mPipeline);

	mMemory = mm->allocateObjectMemory(mPipeline);

	mSlotCount = info.vs.descriptorSetMapping[0].descriptorCount;
}

Pipeline::~Pipeline()
{
}

GR_PIPELINE Pipeline::grPipeline() const
{
	return mPipeline;
}

const GPUMemorySPtr& Pipeline::gpuMemory() const
{
	return mMemory;
}

GR_UINT Pipeline::descriptorSlotCount() const
{
	return mSlotCount;
}

}