#include "GRMaterial.h"

namespace GREngine
{

Material::Material() :
	mDirty(true),
	mPipeline(nullptr)
{
}


Material::~Material()
{
}

std::string & Material::name()
{
	static std::string NAME = "";

	return NAME;
}

bool Material::isDirty() const
{
	MutexGuard locker(mGuard);

	return mDirty;
}

const PipelineSPtr& Material::pipeline() const
{
	return mPipeline;
}

}
