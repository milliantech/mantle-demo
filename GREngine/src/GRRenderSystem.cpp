#include <SDL.h>
#include <assert.h>
#include <stdexcept>
#include <magma.h>

#include <mantleDbg.h>

#include "GRRenderSystem.h"
#include "GRGPUMemoryManager.h"
#include "GRRenderSystemResourceManager.h"
#include "GRMeshManager.h"
#include "GRTextureManager.h"
#include "GRRenderWorker.h"
#include "GRLog.h"
#include "GRQueue.h"

//#define DEBUG

GR_VOID GR_STDCALL debugCallback(GR_ENUM type, GR_ENUM level, GR_BASE_OBJECT obj, GR_SIZE location,
	GR_ENUM msgCode, const GR_CHAR* msg, GR_VOID* userData) {

	GREngine::Log::debug(msg);
}

GR_VOID* GR_STDCALL allocCallback(GR_SIZE size, GR_SIZE alignment, GR_ENUM allocType)
{
	return malloc(size);
}

GR_VOID GR_STDCALL freeCallback(GR_VOID* pMem)
{
	free(pMem);
}

namespace GREngine
{

RenderSystem::RenderSystem():
	mUniversalQueue(nullptr)
{
	init();

	mGPUMemoryManager = new GPUMemoryManager();
	mResourceManager = new RenderSystemResourceManager();
	mMeshManager = new MeshManager();
	mTextureManager = new TextureManager();

	Log::info("RenderSystem created");
}


RenderSystem::~RenderSystem()
{
	delete mTextureManager;
	delete mMeshManager;
	delete mResourceManager;
	delete mGPUMemoryManager;

	shutdown();

	Log::info("RenderSystem destroyed");
}

void RenderSystem::init()
{
	magmaInit();
	grDbgRegisterMsgCallback(debugCallback, nullptr);

	if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0)
		throw std::runtime_error("Could not init SDL Video subsystem");

	GR_ALLOC_CALLBACKS allocCallbacks = {};
	allocCallbacks.pfnAlloc = &allocCallback;
	allocCallbacks.pfnFree = &freeCallback;

	GR_APPLICATION_INFO appInfo = {};
	appInfo.apiVersion = GR_API_VERSION;

	GR_PHYSICAL_GPU gpus[GR_MAX_PHYSICAL_GPUS] = {};
	GR_UINT gpuCount = 0;

	grInitAndEnumerateGpus(&appInfo, nullptr, &gpuCount, gpus);

/*
	GR_SIZE queueTypeCount = 0;
	grGetGpuInfo(gpus[0], GR_INFO_TYPE_PHYSICAL_GPU_QUEUE_PROPERTIES, &queueTypeCount, nullptr);

	GR_PHYSICAL_GPU_QUEUE_PROPERTIES *queueTypeProp = new GR_PHYSICAL_GPU_QUEUE_PROPERTIES[queueTypeCount];
	grGetGpuInfo(gpus[0], GR_INFO_TYPE_PHYSICAL_GPU_QUEUE_PROPERTIES, &queueTypeCount, queueTypeProp);
*/

	// Create device from first compatible GPU
	GR_DEVICE_QUEUE_CREATE_INFO queueInfo = {};
	queueInfo.queueType = GR_QUEUE_UNIVERSAL;
	queueInfo.queueCount = 1;

	assert(grGetExtensionSupport(gpus[0], "GR_WSI_WINDOWS") == GR_SUCCESS);

	static const GR_CHAR* const ppExtensions[] = {
		"GR_WSI_WINDOWS",
	};

	GR_DEVICE_CREATE_INFO deviceInfo = {};
	deviceInfo.queueRecordCount = 1;
	deviceInfo.pRequestedQueues = &queueInfo;
	deviceInfo.extensionCount = 1;
	deviceInfo.ppEnabledExtensionNames = ppExtensions;

#ifdef DEBUG
	deviceInfo.flags |= GR_DEVICE_CREATE_VALIDATION;
	deviceInfo.maxValidationLevel = GR_VALIDATION_LEVEL_4;
#else
	deviceInfo.maxValidationLevel = GR_VALIDATION_LEVEL_0;
#endif

	grCreateDevice(gpus[0], &deviceInfo, &mDevice);

	mUniversalQueue = new Queue(queueInfo.queueType);
}

void RenderSystem::shutdown()
{
	delete mUniversalQueue;
	mDevice = GR_NULL_HANDLE;

	magmaTerminate();

	SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

GR_DEVICE RenderSystem::device() const
{
	return mDevice;
}

Queue& RenderSystem::universalQueue() const
{
	return *mUniversalQueue;
}

}