#include "GRTexture.h"

#include "GRRenderSystem.h"
#include "GRRenderSystemResourceManager.h"
#include "GRGPUMemoryManager.h"
#include "GRGPUMemory.h"
#include "GRTextureManager.h"
#include "GRCommandBuffer.h"

namespace GREngine
{

Texture::Texture()
{
}

Texture::Texture(const GR_IMAGE_CREATE_INFO &imgInfo, GR_ENUM aspect, bool isTarget) :
	mInfo(imgInfo),
	mState(isTarget ? GR_IMAGE_STATE_UNINITIALIZED : GR_IMAGE_STATE_DATA_TRANSFER)
{
	RenderSystem &renderSystem = RenderSystem::getInstance();
	GPUMemoryManager &memoryMgr = GPUMemoryManager::getInstance();

	grCreateImage(renderSystem.device(), &imgInfo, &mImage);

	mMemory = memoryMgr.allocateObjectMemory(mImage);

	mSubresource.aspect = aspect;
	mSubresource.baseMipLevel = 0;
	mSubresource.mipLevels = imgInfo.mipLevels;
	mSubresource.baseArraySlice = 0;
	mSubresource.arraySize = imgInfo.arraySize;

	if (aspect == GR_IMAGE_ASPECT_COLOR)
	{
		GR_IMAGE_VIEW_CREATE_INFO viewInfo = {};
		viewInfo.image = mImage;
		viewInfo.viewType = GR_IMAGE_VIEW_2D;
		viewInfo.format = imgInfo.format;
		viewInfo.channels.r = GR_CHANNEL_SWIZZLE_R;
		viewInfo.channels.g = GR_CHANNEL_SWIZZLE_G;
		viewInfo.channels.b = GR_CHANNEL_SWIZZLE_B;
		viewInfo.channels.a = GR_CHANNEL_SWIZZLE_A;
		viewInfo.subresourceRange = mSubresource;
		viewInfo.minLod = 0.0f;

		grCreateImageView(renderSystem.device(), &viewInfo, &mView);
	}
}

Texture::~Texture()
{
	grBindObjectMemory(mImage, GR_NULL_HANDLE, 0);
	mMemory.reset();

	grDestroyObject(mImage);
}

void Texture::setData(void * data, size_t size)
{
	MutexGuard locker(mGuard);

	GPUMemorySPtr copyMemory = GPUMemoryManager::getInstance().allocateGPUMemory(size, GR_MEMORY_HEAP_CPU_VISIBLE);

	void* imgMem = copyMemory->map();

	memcpy(imgMem, data, size);

	copyMemory->unmap();

	CommandBufferSPtr cmdBuffer = CommandBuffer::getFromPool(GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT);
	GR_CMD_BUFFER copyBuffer = cmdBuffer->grCmdBuffer();

	GR_IMAGE_STATE_TRANSITION transition = {};
	transition.image = mImage;
	transition.oldState = mState;
	transition.newState = GR_IMAGE_STATE_DATA_TRANSFER;
	transition.subresourceRange = mSubresource;

	GR_MEMORY_IMAGE_COPY imageCopy;
	imageCopy.imageSubresource.aspect = mSubresource.aspect;
	imageCopy.imageSubresource.arraySlice = mSubresource.baseArraySlice;
	imageCopy.imageSubresource.mipLevel = mSubresource.baseMipLevel;
	imageCopy.memOffset = 0;
	imageCopy.imageOffset = { 0,0,0 };
	imageCopy.imageExtent = mInfo.extent;

	grCmdPrepareImages(copyBuffer, 1, &transition);

	grCmdCopyMemoryToImage(copyBuffer, copyMemory->grMemory(), mImage, 1, &imageCopy);

	transition.oldState = GR_IMAGE_STATE_DATA_TRANSFER;
	transition.newState = mState;

	grCmdPrepareImages(copyBuffer, 1, &transition);

	cmdBuffer->end();

	MemoryRefSet refs;
	refs.insert(mMemory->grReference());
	refs.insert(copyMemory->grReference(GR_MEMORY_REF_READ_ONLY));

	cmdBuffer->queueBlocking(refs);

	CommandBuffer::returnToPool(cmdBuffer);
}

TextureSPtr Texture::createTexture(const std::string file)
{
	TextureManager &mgr = TextureManager::getInstance();

	return mgr.loadTexture(file);
}

unsigned int Texture::width() const
{
	return mInfo.extent.width;
}

unsigned int Texture::height() const
{
	return mInfo.extent.height;
}

const GPUMemorySPtr& Texture::gpuMemory() const
{
	return mMemory;
}

GR_IMAGE Texture::grImage() const
{
	return mImage;
}

GR_IMAGE_SUBRESOURCE_RANGE Texture::subresource() const
{
	return mSubresource;
}

GR_IMAGE_VIEW Texture::grView() const
{
	return mView;
}

void Texture::setState(GR_ENUM newState, bool blocking)
{
	MutexGuard locker(mGuard);

	CommandBufferSPtr cmdBuff = CommandBuffer::getFromPool(GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT);

	GR_IMAGE_STATE_TRANSITION transition = {};
	transition.image = mImage;
	transition.oldState = mState;
	transition.newState = newState;
	transition.subresourceRange = mSubresource;

	grCmdPrepareImages(cmdBuff->grCmdBuffer(), 1, &transition);

	MemoryRefSet refsSet;
	refsSet.insert(mMemory->grReference());

	if (blocking)
		cmdBuff->queueBlocking(refsSet);
	else
		cmdBuff->queue(refsSet);

	CommandBuffer::returnToPool(cmdBuff);

	mState = newState;
}

GR_ENUM Texture::state() const
{
	MutexGuard locker(mGuard);

	return mState;
}

void Texture::clear(float *color)
{
	MutexGuard locker(mGuard);

	CommandBufferSPtr cmdBuff = CommandBuffer::getFromPool(GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT);

	GR_CMD_BUFFER grCmdBuff = cmdBuff->grCmdBuffer();

	GR_IMAGE_STATE_TRANSITION transition = {};
	transition.image = mImage;
	transition.oldState = mState;
	transition.newState = GR_IMAGE_STATE_CLEAR;
	transition.subresourceRange = mSubresource;

	grCmdPrepareImages(grCmdBuff, 1, &transition);

	if (mSubresource.aspect == GR_IMAGE_ASPECT_COLOR)
		grCmdClearColorImage(grCmdBuff, mImage, color, 1, &mSubresource);
	else
		grCmdClearDepthStencil(grCmdBuff, mImage, 1, 1, 1, &mSubresource);

	transition.oldState = GR_IMAGE_STATE_CLEAR;
	transition.newState = mState;

	grCmdPrepareImages(grCmdBuff, 1, &transition);


	MemoryRefSet refsSet;
	refsSet.insert(mMemory->grReference());

	cmdBuff->queue(refsSet);

	CommandBuffer::returnToPool(cmdBuff);
}

}