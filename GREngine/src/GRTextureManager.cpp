#include <mantle.h>
#include <exception>
#include <FreeImage.h>

#include "GRRenderSystem.h"
#include "GRTextureManager.h"
#include "GRTexture.h"
#include "GRLog.h"

void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message) {
	GREngine::Log(msg);
}

namespace GREngine
{

TextureManager::TextureManager()
{
	mRenderSystem = RenderSystem::getInstancePtr();

	FreeImage_Initialise();
	FreeImage_SetOutputMessage(FreeImageErrorHandler);
}

TextureManager::~TextureManager()
{
	FreeImage_DeInitialise();
}

TextureSPtr TextureManager::loadTexture(const std::string & fileName)
{
	FREE_IMAGE_FORMAT format = FreeImage_GetFIFFromFilename(fileName.c_str());

	FIBITMAP *bitmapRaw = FreeImage_Load(format, fileName.c_str());
	FIBITMAP *bitmap = FreeImage_ConvertTo32Bits(bitmapRaw);

	FreeImage_Unload(bitmapRaw);

	GR_IMAGE_CREATE_INFO imgInfo = {};
	imgInfo.extent.width = FreeImage_GetWidth(bitmap);
	imgInfo.extent.height = FreeImage_GetHeight(bitmap);
	imgInfo.extent.depth = 1;
	imgInfo.format.channelFormat = GR_CH_FMT_R8G8B8A8;
	imgInfo.format.numericFormat = GR_NUM_FMT_UNORM;
	imgInfo.imageType = GR_IMAGE_2D;
	imgInfo.arraySize = 1;
	imgInfo.mipLevels = 1;
	imgInfo.samples = 1;
	imgInfo.tiling = GR_LINEAR_TILING;
	imgInfo.usage = GR_IMAGE_USAGE_SHADER_ACCESS_READ;

	TextureSPtr texture = std::make_shared<Texture>(imgInfo, GR_IMAGE_ASPECT_COLOR, false);
	texture->setData(FreeImage_GetBits(bitmap), FreeImage_GetDIBSize(bitmap));
	texture->setState(GR_IMAGE_STATE_GRAPHICS_SHADER_READ_ONLY);

	FreeImage_Unload(bitmap);

	return texture;
}

}