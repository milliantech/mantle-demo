#include <SDL.h>
#include <chrono>
#include <iostream>
#include <algorithm>
#include <assert.h>

#include "GRScene.h"
#include "GRGPUMemoryManager.h"
#include "GRCommandBuffer.h"
#include "GRGPUMemory.h"
#include "GRBuffer.h"
#include "GRLog.h"
#include "GRQueue.h"
#include "GRSceneObject.h"
#include "GRRenderWorker.h"
#include "GRRenderSystem.h"
#include "GRMemoryRefSet.h"
#include "GRRenderSystemResourceManager.h"
#include "SceneObjectComponent/GRMeshRenderer.h"
#include "SceneObjectComponent/GRCamera.h"
#include "SceneObjectComponent/GRTransform.h"
#include "SceneObjectComponent/GRLight.h"

namespace GREngine
{

using namespace SceneObjectComponents;

struct GPULight
{
	DirectX::XMFLOAT4 color;
	DirectX::XMMATRIX pos;
	float intensity;
	DirectX::XMFLOAT3 padding;
};

struct UpdateContext
{
	std::vector<GPULight> lights;
};

static const size_t MAX_GPU_LIGHTS = 10;

Scene::Scene(const std::string &id) :
	mId(id),
	mActive(false),
	mWorkerThread(nullptr)
{
	mRootSceneObject = std::make_shared<SceneObject>("Root", this, nullptr);

	initGPUMemory();

	setActive(true);

	Log::info("Scene created: " + mId);
}

Scene::~Scene()
{
	setActive(false);

	mRootSceneObject.reset();
	mConstantsBuffer.reset();

	Log::info("Scene destroyed: " + mId);
}

const std::string Scene::getId() const
{
	return mId;
}

bool Scene::active() const
{
	return mActive;
}

void Scene::setActive(bool active)
{
	mActive = active;

	if (isRunning() != active)
	{
		if (active)
			start();
		else
			stop();
	}
}

void Scene::activateViewport(const ViewportSPtr& viewport)
{
	MutexGuard locker(mViewportsMutex);

	mViewport = viewport;
}

void Scene::deactivateViewport(const ViewportSPtr& viewport)
{
	MutexGuard locker(mViewportsMutex);

	mViewport = nullptr;
}

GR_MEMORY_VIEW_ATTACH_INFO Scene::constantsView(MemoryRefSet& refs) const
{
	refs.insert(mConstantsBuffer->grReference(GR_MEMORY_REF_READ_ONLY));

	return mConstantsBuffer->grView({GR_CH_FMT_UNDEFINED, GR_NUM_FMT_UNDEFINED}, sizeof(RenderSceneConstants));
}

GR_MEMORY_VIEW_ATTACH_INFO Scene::lightsView(MemoryRefSet& refs) const
{
	refs.insert(mLightsBuffer->grReference(GR_MEMORY_REF_READ_ONLY));

	return mLightsBuffer->grView({GR_CH_FMT_UNDEFINED, GR_NUM_FMT_UNDEFINED}, sizeof(GPULight));
}

const SceneObjectSPtr& Scene::rootObject() const
{
	return mRootSceneObject;
}

bool Scene::isRunning() const
{
	MutexGuard locker(mRunMutex);

	return mWorkerThread != nullptr;
}

void Scene::stop()
{
	MutexGuard locker(mRunMutex);

	mActive = false;

	mWorkerThread->join();
}

void Scene::start()
{
	MutexGuard locker(mRunMutex);

	mWorkerThread = new std::thread(&Scene::run, this);
}

void Scene::run()
{
	unsigned int lastTime, currentTime, deltaTime = 0;
	lastTime = currentTime = SDL_GetTicks();

	unsigned int tmp, tmp2;
	tmp = tmp2 = SDL_GetTicks();

	while (active())
	{
		update(deltaTime);
		draw();

		currentTime = SDL_GetTicks();
		deltaTime = currentTime - lastTime;
		lastTime = currentTime;

		std::cout << "Scene update time: " << deltaTime << std::endl;
	}

	mWorkerThread = nullptr;
}

void Scene::initGPUMemory()
{
	GR_SIZE memorySize = sizeof(RenderSceneConstants) + (sizeof(GPULight) * MAX_GPU_LIGHTS);

	mGPUMemory = GPUMemoryManager::getInstance().allocateGPUMemory(memorySize, GR_MEMORY_HEAP_CPU_VISIBLE);

	mConstantsBuffer = std::make_shared<Buffer>(mGPUMemory, sizeof(RenderSceneConstants));
	mLightsBuffer = std::make_shared<Buffer>(mGPUMemory, sizeof(GPULight) * MAX_GPU_LIGHTS);
}

void Scene::updateGPUMemory(UpdateContext &updateContext)
{
	MemoryRefSet refsSet;
	refsSet.insert(mGPUMemory->grReference());

	CommandBufferSPtr commandBuff = CommandBuffer::getFromPool();

	mConstantsBuffer->setState(commandBuff, GR_MEMORY_STATE_DATA_TRANSFER);
	mLightsBuffer->setState(commandBuff, GR_MEMORY_STATE_DATA_TRANSFER);

	commandBuff->queueBlocking(refsSet);
	CommandBuffer::returnToPool(commandBuff);

	mConstants.lightsCount = (unsigned int) updateContext.lights.size();

	char *gpuMem = (char *) mGPUMemory->map();

	if(!updateContext.lights.empty())
		memcpy(gpuMem + mLightsBuffer->offset(), updateContext.lights.data(), sizeof(GPULight) * std::min(updateContext.lights.size(), MAX_GPU_LIGHTS));

	memcpy(gpuMem + mConstantsBuffer->offset(), &mConstants, sizeof(RenderSceneConstants));

	mGPUMemory->unmap();

	commandBuff = CommandBuffer::getFromPool();

	mConstantsBuffer->setState(commandBuff, GR_MEMORY_STATE_GRAPHICS_SHADER_READ_ONLY);
	mLightsBuffer->setState(commandBuff, GR_MEMORY_STATE_GRAPHICS_SHADER_READ_ONLY);

	commandBuff->queue(refsSet);
	CommandBuffer::returnToPool(commandBuff);
}

void Scene::update(unsigned int timeInMilis)
{
	UpdateContext context;

	update(context, mRootSceneObject, timeInMilis);

	updateGPUMemory(context);
}

void Scene::update(UpdateContext &context, const SceneObjectSPtr &sceneObject, unsigned int timeInMilis)
{
	std::vector<SceneObjectSPtr> childs;

	if (!sceneObject->active())
		return;

	sceneObject->update(timeInMilis);

	TransformSPtr transform = sceneObject->component<Transform>();
	LightSPtr light = sceneObject->component<Light>();
	if (light)
	{
		GPULight gpuL;
		DirectX::XMStoreFloat4(&gpuL.color, light->color());
		gpuL.intensity = light->intensity();
		gpuL.pos = DirectX::XMMatrixTranspose(transform->matrix());

		context.lights.push_back(gpuL);
	}

	sceneObject->childs(childs);

	for (const SceneObjectSPtr &child : childs)
	{
		update(context, child, timeInMilis);
	}
}

void Scene::draw()
{
	RenderWorkerSPtr renderWorker;
	ViewportSPtr viewport = mViewport;

	RenderSystemResourceManager &resourceManager = RenderSystemResourceManager::getInstance();

	if ((viewport == nullptr) || (viewport->colorTarget() == nullptr || viewport->depthTarget() == nullptr))
		return;

	renderWorker = resourceManager.renderWorker();

	RenderPhaseParamsSPtr phaseParams = std::make_shared<RenderPhaseParams>();
	phaseParams->scene = this;
	phaseParams->viewport = viewport;

	RenderSystem::getInstance().universalQueue().flush();

	renderWorker->renderScene(phaseParams, mRootSceneObject);
	renderWorker->presentResult();

	resourceManager.returnRenderWorker(renderWorker);
}


}
