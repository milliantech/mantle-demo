#include "GRRenderWorker.h"
#include "GRRenderTarget.h"
#include "GRRenderSystem.h"
#include "GRQueue.h"
#include "GRGPUMemoryManager.h"
#include "GRGPUMemory.h"
#include "GRRenderer.h"

#include "SceneObjectComponent\GRMeshRenderer.h"
#include "SceneObjectComponent\GRTransform.h"
#include "GRScene.h"
#include "GRDescriptorSet.h"
#include "GRMemoryRefSet.h"
#include "GRCommandBuffer.h"
#include "GRRenderTree.h"
#include "GRSceneObject.h"
#include "GRPipeline.h"
#include "GRViewport.h"

#include "GRUtils.h"

namespace GREngine
{

using namespace SceneObjectComponents;

class RenderThread
{
public:
	RenderThread(RenderWorker *worker) :
		mWorker(worker)
	{
		mThread = new std::thread(&RenderThread::run, this);
	}

	~RenderThread()
	{
		delete mThread;
	}

	CommandBufferSPtr prepareRenderCommand(const RenderNodeSPtr &node, MemoryRefSet &memRefs)
	{
		CommandBufferSPtr cmdBuff = CommandBuffer::getFromPool(GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT | GR_CMD_BUFFER_OPTIMIZE_GPU_SMALL_BATCH | GR_CMD_BUFFER_OPTIMIZE_DESCRIPTOR_SET_SWITCH);

		const ViewportSPtr &viewport = mWorker->mPhaseParams->viewport;
		const PipelineSPtr &pipeline = node->pipeline;
		const ViewportStateSPtr& viewportState = viewport->state();

		GR_CMD_BUFFER grCmdBuff = cmdBuff->grCmdBuffer();

		GR_COLOR_TARGET_BIND_INFO colorTargetBindInfo;
		colorTargetBindInfo.view = viewport->colorTarget()->colorTargetView();
		colorTargetBindInfo.colorTargetState = GR_IMAGE_STATE_TARGET_RENDER_ACCESS_OPTIMAL;

		GR_DEPTH_STENCIL_BIND_INFO depthTargetBindInfo;
		depthTargetBindInfo.depthState = GR_IMAGE_STATE_TARGET_SHADER_ACCESS_OPTIMAL;
		depthTargetBindInfo.stencilState = GR_IMAGE_STATE_TARGET_SHADER_ACCESS_OPTIMAL;
		depthTargetBindInfo.view = viewport->depthTarget()->depthStencilTargetView();

		grCmdBindTargets(grCmdBuff, 1, &colorTargetBindInfo, &depthTargetBindInfo);

		grCmdBindStateObject(grCmdBuff, GR_STATE_BIND_MSAA, viewportState->msaa);
		grCmdBindStateObject(grCmdBuff, GR_STATE_BIND_VIEWPORT, viewportState->viewport);
		grCmdBindStateObject(grCmdBuff, GR_STATE_BIND_COLOR_BLEND, viewportState->colorBlend);
		grCmdBindStateObject(grCmdBuff, GR_STATE_BIND_DEPTH_STENCIL, viewportState->depthStencil);
		grCmdBindStateObject(grCmdBuff, GR_STATE_BIND_RASTER, viewportState->raster);

		grCmdBindPipeline(grCmdBuff, GR_PIPELINE_BIND_POINT_GRAPHICS, pipeline->grPipeline());

		memRefs.insert(pipeline->gpuMemory()->grReference(GR_MEMORY_REF_READ_ONLY));
		viewport->texturesRefs(memRefs);

		return cmdBuff;
	}

	void run()
	{
		while(mWorker->mActive)
		{
			mWorker->fetchNode(mCurrentNode);

			if (mCurrentNode == nullptr)
				continue;

			MemoryRefSet refs;

			CommandBufferSPtr updateCommand = CommandBuffer::getFromPool(GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT);
			CommandBufferSPtr renderCommand = prepareRenderCommand(mCurrentNode, refs);

			const GR_UINT descriptorSlotCount = mCurrentNode->pipeline->descriptorSlotCount();

			DescriptorSetSPtr descriptorSet = DescriptorSet::getFromPool(descriptorSlotCount * RenderNode::maxBatchCount());
			refs.insert(descriptorSet->gpuMemory()->grReference(GR_MEMORY_REF_READ_ONLY));

			descriptorSet->beginUpdate();

			GR_UINT descriptorOffset = 0;
			for (const RenderContext &context : mCurrentNode->childs)
			{
				context.renderer->updateGPUMemory(updateCommand, mWorker->mPhaseParams, context);

				context.renderer->bindData(refs, descriptorSet, descriptorOffset, mWorker->mPhaseParams, context);

				descriptorOffset += descriptorSlotCount;
			}

			descriptorSet->endUpdate();

			descriptorOffset = 0;
			for (const RenderContext &context : mCurrentNode->childs)
			{
				descriptorSet->bind(renderCommand, 0, descriptorOffset);

				context.renderer->draw(renderCommand, mWorker->mPhaseParams, context);
				
				descriptorOffset += descriptorSlotCount;
			}

			updateCommand->end();
			renderCommand->end();

			updateCommand->queue(refs);
			renderCommand->queue(refs);
			
			/*
			std::vector<GR_CMD_BUFFER> commands;
			commands.push_back(updateCommand->grCmdBuffer());
			commands.push_back(renderCommand->grCmdBuffer());
			
			RenderSystem::getInstance().universalQueue().queueCommand(commands, refs);
			*/
		
			CommandBuffer::returnToPool(updateCommand);
			CommandBuffer::returnToPool(renderCommand);
			DescriptorSet::returnToPool(descriptorSet);

			mCurrentNode = nullptr;

			mWorker->endNode();
		}
	}

	void join()
	{
		mThread->join();
	}

public:
	RenderWorker *mWorker;

	std::thread *mThread;
	RenderNodeSPtr mCurrentNode;
};

RenderWorker::RenderWorker() :
	mActive(true)
{
	mTree = std::make_shared<RenderTree>();
	mCurrentNode = mTree->end();

	initRenderThreads(4);
}

RenderWorker::~RenderWorker()
{
	mActive = false;

	stopRenderThreads();
}

void RenderWorker::initRenderThreads(size_t count)
{
	for (size_t i = 0; i < count; ++i)
	{
		RenderThreadSPtr thread = std::make_shared<RenderThread>(this);
		mThreads.push_back(thread);
	}
}

void RenderWorker::stopRenderThreads()
{
	mTreeMutex.lock();

	mTree = std::make_shared<RenderTree>();
	mCurrentNode = mTree->end();

	mTreeMutex.unlock();

	mTreeCondition.notify_all();
	mJoinCondition.notify_all();

	for (const RenderThreadSPtr &renderThread : mThreads)
	{
		renderThread->join();
	}
}

void RenderWorker::renderScene(const RenderPhaseParamsSPtr &params, const SceneObjectSPtr &rootObj)
{
	mPhaseParams = params;

 	beginTraverse();

	RenderContext context;
	context.parentTransform = DirectX::XMMatrixIdentity();

	traverseScene(context, rootObj);

	endTraverse();

	beginRender();
}

void RenderWorker::presentResult()
{
	joinRender();

	mPhaseParams->viewport->colorTarget()->present();
}

void RenderWorker::joinRender()
{
	std::unique_lock<std::mutex> lock(mJoinConditionMutex);
	while (isRenderRunning() && mActive)
	{
		mJoinCondition.wait(lock);
	}
}

void RenderWorker::fetchNode(RenderNodeSPtr &node)
{
	std::unique_lock<std::mutex> waitLock(mTreeCondMutex);
	while (isCurrentAtEnd() && mActive)
		mTreeCondition.wait(waitLock);

	MutexGuard treeLock(mTreeMutex);

	do
	{
		if (mCurrentNode == mTree->end())
		{
			node = nullptr;
			break;
		}

		node = *mCurrentNode;

		mCurrentNode++;
	} 
	while (node->childs.size() == 0);
}

void RenderWorker::endNode()
{
	if (!isRenderRunning())
		endRender();
}

bool RenderWorker::isCurrentAtEnd() const
{
	MutexGuard queueLock(mTreeMutex);

	return mCurrentNode == mTree->end();
}

void RenderWorker::beginTraverse()
{
	mTree = std::make_shared<RenderTree>();
	mCurrentNode = mTree->end();
}

void RenderWorker::traverseScene(const RenderContext &context, const SceneObjectSPtr &sceneObject)
{
	std::vector<SceneObjectSPtr> childs;
	std::vector<RendererSPtr> renderers;

	if (!sceneObject->active())
		return;

	sceneObject->childs(childs);

	RenderContext childContext = context;

	for (const SceneObjectSPtr &child : childs)
	{
		renderers.clear();
		child->components<Renderer>(renderers);

		for (const RendererSPtr &renderer : renderers)
		{
			childContext.renderer = renderer;

			mTree->add(childContext);
		}

		traverseScene(childContext, child);
	}
}

void RenderWorker::endTraverse()
{
}

void RenderWorker::beginRender()
{
	const RenderTargetSPtr &colorTarget = mPhaseParams->viewport->colorTarget();
	const RenderTargetSPtr &depthTarget = mPhaseParams->viewport->depthTarget();

	mPhaseParams->viewport->update();

	if(colorTarget != nullptr)
	{
		colorTarget->prepare();
		colorTarget->clear();
	}

	if(depthTarget != nullptr)
	{
		depthTarget->prepare();
		depthTarget->clear();
	}

	mTreeCondMutex.lock();

	mCurrentNode = mTree->begin();

	mTreeCondMutex.unlock();

	mTreeCondition.notify_all();
}

void RenderWorker::endRender()
{
	mJoinCondition.notify_all();
}

bool RenderWorker::isRenderRunning() const
{
	MutexGuard queueLock(mTreeMutex);

	if (mCurrentNode != mTree->end())
		return true;

	for (const RenderThreadSPtr &thread : mThreads)
	{
		if (thread->mCurrentNode != nullptr)
			return true;
	}

	return false;
}

/*

bool RenderWorker::isRenderActive() const
{
	MutexGuard locker(mRenderRunningMutex);

	return mRenderActive;
}

bool RenderWorker::isRenderRunning() const
{
	MutexUniqueLock queueLock(mRenderQueueMutex, std::defer_lock);
	MutexUniqueLock runnginLock(mRenderRunningMutex, std::defer_lock);
	
	std::lock(queueLock, runnginLock);

	if (!mRenderQueue.empty())
		return true;

	for (const RenderThreadSPtr &thread : mRenderThreads)
	{
		if (thread->mRunning)
			return true;
	}

	return false;
}

void RenderWorker::join()
{
	std::unique_lock<std::mutex> lock(mRunningConditionMutex);
	while (isRenderRunning())
	{
		mRenderJoinCondition.wait(lock);
	}
}

void RenderWorker::queueRenderCommand(const RenderContext &context)
{
	MutexUniqueLock queueLock(mRenderQueueMutex, std::defer_lock);
	MutexUniqueLock condLock(mQueueCondigionMutex, std::defer_lock);

	std::lock(queueLock, condLock);

	mRenderQueue.push(context);

	queueLock.unlock();
	condLock.unlock();

	mRenderQueueCondition.notify_one();
}

void RenderWorker::fetchCommand(RenderContext &context, bool &running)
{
	std::unique_lock<std::mutex> waitLock(mQueueCondigionMutex);
	while (isQueueEmpty())
		mRenderQueueCondition.wait(waitLock);

	MutexUniqueLock runningLock(mRenderRunningMutex, std::defer_lock);
	MutexUniqueLock queueLock(mRenderQueueMutex, std::defer_lock);

	std::lock(runningLock, queueLock);

	running = true;

	context = mRenderQueue.front();
	mRenderQueue.pop();
}

void RenderWorker::finishCommand(bool &running)
{
	MutexUniqueLock runningLock(mRenderRunningMutex, std::defer_lock);
	MutexUniqueLock runningCondLock(mRunningConditionMutex, std::defer_lock);

	std::lock(runningLock, runningCondLock);

	running = false;

	for (const RenderThreadSPtr &thread : mRenderThreads)
	{
		if (thread->mRunning)
			return;
	}

	runningLock.unlock();
	runningCondLock.unlock();

	mRenderJoinCondition.notify_all();
}

bool RenderWorker::isQueueEmpty() const
{
	MutexGuard locker(mRenderQueueMutex);

	return mRenderQueue.empty();
}

void RenderWorker::beginRender()
{
	mRenderActive = true;
	
	runningLock.unlock();

	const RenderTargetSPtr &colorTarget = mViewport->colorTarget();
	const RenderTargetSPtr &depthTarget = mViewport->depthTarget();

	mRenderQueue.swap(std::queue<RenderContext>());

	colorTarget->prepare();
	colorTarget->clear();

	depthTarget->prepare();
	depthTarget->clear();
}


void RenderWorker::presentResult()
{

	mRenderParams->viewport->colorTarget()->present();
}
*/
}