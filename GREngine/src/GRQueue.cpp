#include <chrono>
#include <thread>

#include "GRQueue.h"
#include "GRRenderSystem.h"
#include "GRRenderSystemResourceManager.h"
#include "GRLog.h"

namespace GREngine
{

using namespace std::literals;

Queue::Queue(GR_ENUM type) :
	mActive(true),
	mSubmitThread(nullptr)
{
	mRenderSystem = RenderSystem::getInstancePtr();

	grGetDeviceQueue(mRenderSystem->device(), type, 0, &mDeviceQueue);

	GR_FENCE_CREATE_INFO fenceCreateInfo = {};
	grCreateFence(mRenderSystem->device(), &fenceCreateInfo, &mFlushFence);

	GR_CMD_BUFFER_CREATE_INFO bufferCreateInfo = { type, 0 };
	grCreateCommandBuffer(mRenderSystem->device(), &bufferCreateInfo, &mFlushCommand);
	grBeginCommandBuffer(mFlushCommand, 0);
	grEndCommandBuffer(mFlushCommand);

	//mSubmitThread = new std::thread(&Queue::run, this);
}

Queue::~Queue()
{
	mActive = false;

	//mSubmitThread->join();

	//delete mSubmitThread;

	grDestroyObject(mFlushFence);
}

void Queue::queueCommand(GR_CMD_BUFFER command, const MemoryRefSet & releatedMemory, GR_FENCE fence)
{
	/*Entry entry = {};
	entry.cmdBuff = command;
	entry.fence = fence;
	entry.memRefs = new std::vector<GR_MEMORY_REF>();
	entry.wsiInfo = nullptr;

	releatedMemory.items(*entry.memRefs);

	mQueueMutex.lock();

	mQueue.push(entry);

	mQueueMutex.unlock();
	*/

	std::vector<GR_MEMORY_REF> refs;
	releatedMemory.items(refs);

	mQueueMutex.lock();

	if (grQueueSubmit(mDeviceQueue, 1, &command, (GR_UINT) refs.size(), refs.data(), fence) != GR_SUCCESS)
		Log::error("Could not submit command into queue");
	
	mQueueMutex.unlock();
}

void Queue::wsiPresent(GR_WSI_WIN_PRESENT_INFO * presentInfo)
{
	/*Entry entry = {};
	entry.cmdBuff = GR_NULL_HANDLE;
	entry.fence = GR_NULL_HANDLE;
	entry.memRefs = nullptr;
	entry.wsiInfo = presentInfo;

	mQueueMutex.lock();

	mQueue.push(entry);

	mQueueMutex.unlock();*/

	mQueueMutex.lock();

	grWsiWinQueuePresent(mDeviceQueue, presentInfo);

	mQueueMutex.unlock();
}


void Queue::queueCommandBlocking(GR_CMD_BUFFER command, const MemoryRefSet & releatedMemory)
{
	GR_FENCE fence = GR_NULL_HANDLE;

	GR_FENCE_CREATE_INFO fenceCreateInfo = {};
	grCreateFence(mRenderSystem->device(), &fenceCreateInfo, &fence);

	queueCommand(command, releatedMemory, fence);

	grWaitForFences(mRenderSystem->device(), 1, &fence, true, -1);
	grDestroyObject(fence);

	RenderSystemResourceManager::getInstance().flush();
}

void Queue::run()
{
	Entry entry;

	while (mActive)
	{
		while (mActive && isQueueEmpty())
		{
			std::this_thread::sleep_for(1ms);
		}

		if (!mActive)
			break;

		mQueueMutex.lock();

		entry = mQueue.front();

		mQueue.pop();

		mQueueMutex.unlock();

		if (entry.wsiInfo == nullptr)
			grQueueSubmit(mDeviceQueue, 1, &entry.cmdBuff, (GR_UINT)entry.memRefs->size(), entry.memRefs->data(), entry.fence);
		else
			grWsiWinQueuePresent(mDeviceQueue, entry.wsiInfo);
			
		delete entry.memRefs;
	}
}

bool Queue::isQueueEmpty() const
{
	mQueueMutex.lock();

	bool empty = mQueue.empty();

	mQueueMutex.unlock();

	return empty;
}

void Queue::flush()
{
	MutexGuard locker(mFlushMutex);

	MemoryRefSet refs;

	queueCommand(mFlushCommand, refs, mFlushFence);

	grWaitForFences(mRenderSystem->device(), 1, &mFlushFence, true, -1);

	RenderSystemResourceManager::getInstance().flush();
}

}