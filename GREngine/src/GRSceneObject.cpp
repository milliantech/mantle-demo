#include <algorithm>

#include "GRSceneObject.h"
#include "GRSceneObjectComponent.h"

#include "SceneObjectComponent\GRTransform.h"

namespace GREngine 
{

SceneObject::SceneObject(const std::string &name, Scene *scene, SceneObject *parent) :
	mName(name),
	mScene(scene),
	mParent(parent),
	mActive(true)
{
	mTransform = std::make_shared<SceneObjectComponents::Transform>(this);
}

SceneObject::~SceneObject()
{
	mComponents.clear();
	mTransform.reset();
}

const std::string & SceneObject::name() const
{
	return mName;
}

bool SceneObject::active() const
{
	return mActive;
}

void SceneObject::setActive(bool active)
{
	if(active != mActive)
	{
		mActive = active;

		if(mParent != nullptr)
			mParent->setActive(active);
	}
}

void SceneObject::components(std::vector<SceneObjectComponentSPtr> &components)
{
	MutexGuard locker(mComponentsMutex);

	components = mComponents;
}

SceneObjectSPtr SceneObject::createChild(const std::string &name)
{
	SceneObjectSPtr obj = std::make_shared<SceneObject>(name, mScene, this);

	MutexGuard locker(mChildsMutex);

	mChilds.push_back(obj);

	return obj;
}

void SceneObject::removeChild(const SceneObjectSPtr &child)
{
	MutexGuard locker(mChildsMutex);

	std::remove(mChilds.begin(), mChilds.end(), child);
}

const SceneObjectSPtr& SceneObject::child(size_t index) const
{
	MutexGuard locker(mChildsMutex);

	return mChilds.at(index);
}

void SceneObject::childs(std::vector<SceneObjectSPtr> &childs) const
{
	MutexGuard locker(mChildsMutex);

	childs = mChilds;
}

void SceneObject::update(unsigned int timeInMilis)
{
	std::vector<SceneObjectComponentSPtr> comps;
	components(comps);

	for (const SceneObjectComponentSPtr &component : comps)
	{
		if (!component->active())
			continue;

		component->update(timeInMilis);
	}
}

}
