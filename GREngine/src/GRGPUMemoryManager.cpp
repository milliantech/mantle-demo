#include <iostream>
#include <algorithm>

#include "GRGPUMemoryManager.h"
#include "GRRenderSystem.h"
#include "GRGPUMemory.h"
#include "GRBuffer.h"

const GR_UINT POOL_ITEM_SIZE = 4 * 1024; //4KB

namespace GREngine
{

GPUMemoryManager::GPUMemoryManager()
{
	mRenderSystem = RenderSystem::getInstancePtr();

	GR_UINT heapCount;
	grGetMemoryHeapCount(mRenderSystem->device(), &heapCount);

	mHeapProps.reserve(heapCount);

	GR_MEMORY_HEAP_PROPERTIES heapProps = {};
	GR_SIZE heapPropsSize = sizeof(heapProps);

	for (unsigned int i = 0; i < heapCount; i++) 
	{
		heapProps = {};

		grGetMemoryHeapInfo(mRenderSystem->device(), i, GR_INFO_TYPE_MEMORY_HEAP_PROPERTIES, &heapPropsSize, &heapProps);

		mHeapProps.push_back(heapProps);
	}
}


GPUMemoryManager::~GPUMemoryManager()
{
	mHeapProps.clear();
}

GPUMemorySPtr GPUMemoryManager::allocateObjectMemory(const GR_OBJECT &obj, const GR_ENUM priority)
{
	GR_MEMORY_REQUIREMENTS memReqs = {};
	GR_SIZE memReqsSize = sizeof(memReqs);
	grGetObjectInfo(obj, GR_INFO_TYPE_MEMORY_REQUIREMENTS, &memReqsSize, &memReqs);
	
	std::vector<GR_UINT> heapIds;
	heapIds.assign(memReqs.heaps, memReqs.heaps + memReqs.heapCount);

	GR_MEMORY_ALLOC_INFO allocInfo = {};
	createAllocInfo(heapIds, memReqs.size, 0, priority, memReqs.alignment, allocInfo);

	GPUMemorySPtr memory = std::make_shared<GPUMemory>(allocInfo);

	grBindObjectMemory(obj, memory->grMemory(), 0);

	return memory;
}

GPUMemorySPtr GPUMemoryManager::allocateGPUMemory(const GR_GPU_SIZE size, const GR_FLAGS flags, const GR_ENUM priority, const GR_GPU_SIZE alignment)
{
	std::vector<GR_UINT> heapIds;

	findSuitableHeaps(flags, heapIds);

	GR_MEMORY_ALLOC_INFO allocInfo = {};
	createAllocInfo(heapIds, size, flags, priority, alignment, allocInfo);

	GPUMemorySPtr memory = std::make_shared<GPUMemory>(allocInfo);

	return memory;
}

BufferSPtr GPUMemoryManager::allocateBufferFromGPUPool(const GR_GPU_SIZE size, const GR_FLAGS flags, const GR_ENUM priority)
{
	MutexGuard locker(mPoolMutex);

	GR_MEMORY_ALLOC_INFO memoryInfo;
	GPUMemory::ObjectAllocInfo submemInfo;

	for (const GPUMemorySPtr memory : mMemoryPool)
	{
		memoryInfo = memory->grInfo();

		if ((memoryInfo.flags & flags) == flags && memoryInfo.memPriority == priority)
		{
			if (memory->suballocate(size, submemInfo))
			{
				return std::make_shared<Buffer>(memory, submemInfo);
			}
		}
	}

	GPUMemorySPtr memory = allocateGPUMemory(POOL_ITEM_SIZE, flags, priority);

	mMemoryPool.push_back(memory);

	return std::make_shared<Buffer>(memory, size);
}

void GPUMemoryManager::findSuitableHeaps(const GR_FLAGS flags, std::vector<GR_UINT> &heapIds) const
{
	GR_UINT heapId = 0;

	for (const GR_MEMORY_HEAP_PROPERTIES &heapProps : mHeapProps)
	{
		if ((heapProps.flags & flags) == flags) 
		{	
			heapIds.push_back(heapId);
		}

		heapId++;
	}
}

void GPUMemoryManager::createAllocInfo(std::vector<GR_UINT>& heapIds, GR_GPU_SIZE size, GR_FLAGS flags,GR_ENUM priority, GR_GPU_SIZE alignment,
	GR_MEMORY_ALLOC_INFO & allocInfo) const
{
	std::sort(heapIds.begin(), heapIds.end(),
		[this, size, flags](const GR_UINT& a, const GR_UINT& b)
	{
		float aRate = 0.0f;

		const GR_MEMORY_HEAP_PROPERTIES aProps = mHeapProps[a];
		const GR_MEMORY_HEAP_PROPERTIES bProps = mHeapProps[b];

		aRate += aProps.gpuWritePerfRating - bProps.gpuWritePerfRating + aProps.gpuReadPerfRating - bProps.gpuReadPerfRating;
		if (flags & GR_MEMORY_HEAP_CPU_VISIBLE)
		{
			aRate += aProps.cpuWritePerfRating - bProps.cpuWritePerfRating + aProps.cpuReadPerfRating - bProps.cpuReadPerfRating;
		}

		return aRate > 0;
	});

	std::vector<GR_UINT>::iterator maxHeapId = std::max_element(heapIds.begin(), heapIds.end(),
		[this](const GR_UINT &a, const GR_UINT &b) 
	{
		return mHeapProps[a].pageSize < mHeapProps[b].pageSize;
	});

	GR_MEMORY_HEAP_PROPERTIES maxHeapProps = mHeapProps[*maxHeapId];

	allocInfo.size = (1 + size / maxHeapProps.pageSize) * maxHeapProps.pageSize;
	allocInfo.alignment = (1 + alignment / maxHeapProps.pageSize) * maxHeapProps.pageSize;
	allocInfo.memPriority = priority;
	allocInfo.heapCount = heapIds.size();
	memcpy(allocInfo.heaps, heapIds.data(), heapIds.size() * sizeof(GR_UINT));
}

}