#include "GRSceneObjectComponent.h"

namespace GREngine
{

SceneObjectComponent::SceneObjectComponent(SceneObject *sceneObj) :
	mSceneObject(sceneObj),
	mActive(true)
{
}

SceneObjectComponent::~SceneObjectComponent()
{
}

void SceneObjectComponent::update(unsigned int timeInMilis)
{
}

SceneObject* SceneObjectComponent::sceneObject()
{
	return mSceneObject;
}

bool SceneObjectComponent::active() const
{
	return mActive;
}

void SceneObjectComponent::setActive(bool active)
{
	mActive = active;
}

}