#include "GRGPUMemory.h"

#include "GRRenderSystem.h"
#include "GRRenderSystemResourceManager.h"
#include "GRCommandBuffer.h"

#include <algorithm>

namespace GREngine
{

GPUMemory::GPUMemory(const GR_MEMORY_ALLOC_INFO &info) :
	mInfo(info),
	mExternal(false)
{
	RenderSystem &rs = RenderSystem::getInstance();

	grAllocMemory(rs.device(), &info, &mMemory);
}

GPUMemory::GPUMemory(const GR_GPU_MEMORY & memory) :
	mInfo({}),
	mExternal(true)
{
	mMemory = memory;
}

GPUMemory::~GPUMemory()
{
	if (!mExternal)
		grFreeMemory(mMemory);
}

GR_GPU_MEMORY GPUMemory::grMemory() const
{
	return mMemory;
}

GR_MEMORY_REF GPUMemory::grReference(const GR_FLAGS flags) const
{
	GR_MEMORY_REF ref = {};
	ref.mem = mMemory;
	ref.flags = flags;

	return ref;
}

const GR_MEMORY_ALLOC_INFO& GPUMemory::grInfo() const
{
	return mInfo;
}

bool GPUMemory::suballocate(const GR_GPU_SIZE range, GPUMemory::ObjectAllocInfo &subInfo)
{
	subInfo = {};

	std::vector<ObjectAllocInfo>::iterator it = mSubMems.begin();
	std::vector<ObjectAllocInfo>::iterator next = mSubMems.begin();
	
	if (next != mSubMems.end())
		++next;

	//Find free space between two neighbour buffers
	for (; next != mSubMems.end(); ++it, ++next)
	{
		if (next->offset - (it->offset + it->range) >= range)
			break;
	}

	if (it == mSubMems.end())
	{
		if (mInfo.size < range)
			return false;

		subInfo.offset = 0;
		subInfo.range = range;

		mSubMems.push_back(subInfo);

		return true;
	}
	else if (it->offset + it->range + range < mInfo.size)
	{
		subInfo.offset = it->offset + it->range;
		subInfo.range = range;

		mSubMems.insert(++it, subInfo);

		return true;
	}

	return false;
}

void GPUMemory::desuballocate(const GPUMemory::ObjectAllocInfo & info)
{
	std::remove_if(mSubMems.begin(), mSubMems.end(), [info] (const ObjectAllocInfo &subMem) {
		return info.offset == subMem.offset && info.range == subMem.range;
	});
}

void * GPUMemory::map() const
{
	void *buffer = nullptr;
	grMapMemory(mMemory, 0, &buffer);

	return buffer;
}

void GPUMemory::unmap() const
{
	grUnmapMemory(mMemory);
}

}