#include "GRDescriptorSet.h"

#include "GRRenderSystem.h"
#include "GRRenderSystemResourceManager.h"
#include "GRGPUMemory.h"
#include "GRCommandBuffer.h"
#include "GRGPUMemoryManager.h"

namespace GREngine
{

	DescriptorSetSPtr DescriptorSet::getFromPool(const GR_UINT slotCount)
	{
		return RenderSystemResourceManager::getInstance().getDescriptorSet(slotCount);
	}

	void DescriptorSet::returnToPool(DescriptorSetSPtr descriptorSet)
	{
		RenderSystemResourceManager::getInstance().freeDescriptorSet(descriptorSet);
	}

DescriptorSet::DescriptorSet(const GR_DESCRIPTOR_SET_CREATE_INFO &info) :
	mSlotCount(info.slots)
{
	RenderSystem &rs = RenderSystem::getInstance();
	GPUMemoryManager &memoryMgr = GPUMemoryManager::getInstance();

	grCreateDescriptorSet(rs.device(), &info, &mDescriptorSet);

	mMemory = memoryMgr.allocateObjectMemory(mDescriptorSet, GR_MEMORY_PRIORITY_VERY_HIGH);
}

DescriptorSet::~DescriptorSet()
{
	grBindObjectMemory(mDescriptorSet, GR_NULL_HANDLE, 0);
	mMemory.reset();

	grDestroyObject(mDescriptorSet);
}

GR_DESCRIPTOR_SET DescriptorSet::grDescriptorSet() const
{
	return mDescriptorSet;
}

const GPUMemorySPtr& DescriptorSet::gpuMemory() const
{
	return mMemory;
}

void DescriptorSet::beginUpdate()
{
	grBeginDescriptorSetUpdate(mDescriptorSet);
}

void DescriptorSet::endUpdate()
{
	grEndDescriptorSetUpdate(mDescriptorSet);
}

void DescriptorSet::bind(CommandBufferSPtr cmdBuff, GR_UINT index, GR_UINT offset, GR_ENUM target)
{
	grCmdBindDescriptorSet(cmdBuff->grCmdBuffer(), target, index, mDescriptorSet, offset);

}

GR_UINT DescriptorSet::slotCount() const
{
	return mSlotCount;
}

void DescriptorSet::reset()
{
	//grClearDescriptorSetSlots(mDescriptorSet, 0, mSlotCount);
}

}