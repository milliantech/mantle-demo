from struct import *
import sys

code = []

with open(sys.argv[1]) as f:
	for line in f:
		if not "//" in line:
			continue

		hex = line.strip()[-8:];

		i = 6
		while i >= 0 :
			half = hex[i:i+2]
			code.append(int(half, 16))
			i -= 2

with open(sys.argv[2], 'wb') as f:
    f.write(pack("B" * len(code), *code))