#pragma once

#include <iostream>
#include <SDL.h>

#define PROFILE_START() {\
	unsigned int startTimeProfileVar, midTimeProfileVar, tmpTimeProfileVar;\
	startTimeProfileVar = midTimeProfileVar = tmpTimeProfileVar = SDL_GetTicks();

#define PROFILE_END(MESSAGE) \
  tmpTimeProfileVar = SDL_GetTicks(); \
  std::cout << MESSAGE << ":" << tmpTimeProfileVar - startTimeProfileVar << " " << tmpTimeProfileVar - midTimeProfileVar << std::endl; \
}

#define PROFILE_MID(MESSAGE) \
  tmpTimeProfileVar = SDL_GetTicks(); \
  std::cout << MESSAGE << ":" << tmpTimeProfileVar - midTimeProfileVar << std::endl; \
  midTimeProfileVar = tmpTimeProfileVar;