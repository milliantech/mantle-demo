#pragma once

#include "mantle.h"
#include <vector>

#include "GRPrereq.h"


namespace GREngine
{
	class GPUMemory
	{
	public:

		struct ObjectAllocInfo{
			GR_GPU_SIZE offset;
			GR_GPU_SIZE range;
		};

		explicit GPUMemory(const GR_MEMORY_ALLOC_INFO &info);
		explicit GPUMemory(const GR_GPU_MEMORY &memory);
		~GPUMemory();

		GR_GPU_MEMORY grMemory() const;
		GR_MEMORY_REF grReference(const GR_FLAGS = 0) const;
		const GR_MEMORY_ALLOC_INFO& grInfo() const;

		bool suballocate(const GR_GPU_SIZE range, ObjectAllocInfo &allocInfo);
		void desuballocate(const ObjectAllocInfo &info);

		void* map() const;
		void unmap() const;

	private:
		mutable std::mutex mGuard;
		
		std::vector<ObjectAllocInfo> mSubMems;

		GR_GPU_MEMORY mMemory;

		GR_MEMORY_ALLOC_INFO mInfo;

		bool mExternal;
	};

}