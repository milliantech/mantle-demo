#pragma once

#include "mantle.h"

#include "GRPrereq.h"

namespace GREngine
{
	class DescriptorSet
	{
	public:
		static DescriptorSetSPtr getFromPool(const GR_UINT slotCount);
		static void returnToPool(DescriptorSetSPtr descriptorSet);

		explicit DescriptorSet(const GR_DESCRIPTOR_SET_CREATE_INFO &info);
		~DescriptorSet();

		GR_DESCRIPTOR_SET grDescriptorSet() const;

		const GPUMemorySPtr& gpuMemory() const;

		void beginUpdate();
		void endUpdate();

		void bind(CommandBufferSPtr cmdBuff, GR_UINT index, GR_UINT offset, GR_ENUM target = GR_PIPELINE_BIND_POINT_GRAPHICS);

		GR_UINT slotCount() const;

		void reset();

	private:
		GR_DESCRIPTOR_SET mDescriptorSet;

		GPUMemorySPtr mMemory;

		GR_UINT mSlotCount;
	};

}