#pragma once

#include <mantle.h>

#include "GRPrereq.h"
#include "GRSingleton.h"
#include "GRMemoryRefSet.h"

namespace GREngine
{
	class GRExport RenderSystem : public Singleton<RenderSystem>
	{
	public:
		RenderSystem();
		~RenderSystem();

		GR_DEVICE device() const;

		Queue& universalQueue() const;

	private:
		void init();
		void shutdown();

	private:
		RenderSystemResourceManager *mResourceManager;
		GPUMemoryManager *mGPUMemoryManager;
		MeshManager *mMeshManager;
		TextureManager *mTextureManager;

		GR_DEVICE mDevice;

		Queue *mUniversalQueue;
	};

}