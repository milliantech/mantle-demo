#pragma once

#include <set>

#include "GRPrereq.h"

namespace GREngine
{ 
	template<typename T>
	class Attachment
	{
	public:
		Attachment();
		virtual ~Attachment();

		void attach(const std::weak_ptr<T> ptr);
		bool isAttached(const std::weak_ptr<T> ptr);
		void detach(const std::weak_ptr<T> ptr);

		void removeInvalid();
		void clear();

		std::set<std::shared_ptr<T>> items();

	private:
		std::mutex mGuard;

		std::set<std::weak_ptr<T>, std::owner_less<std::weak_ptr<T>>> mAttachments;
	};

	template<typename T>
	inline Attachment<T>::Attachment()
	{
	}

	template<typename T>
	inline Attachment<T>::~Attachment()
	{
	}

	template<typename T>
	inline void Attachment<T>::attach(const std::weak_ptr<T> ptr)
	{
		MutexGuard locker(mGuard);

		mAttachments.insert(ptr);
	}

	template<typename T>
	inline bool Attachment<T>::isAttached(const std::weak_ptr<T> ptr)
	{
		MutexGuard locker(mGuard);

		return mAttachments.find(ptr) != mAttachments.end();
	}

	template<typename T>
	inline void Attachment<T>::detach(const std::weak_ptr<T> ptr)
	{
		MutexGuard locker(mGuard);

		mAttachments.erase(ptr);
	}

	template<typename T>
	inline void Attachment<T>::removeInvalid()
	{
		MutexGuard locker(mGuard);

		//std::remove_if(mAttachments.begin(), mAttachments.end(), [](const std::weak_ptr<T> &ptr) {return ptr.lock() == nullptr; });
	}

	template<typename T>
	inline void Attachment<T>::clear()
	{
		MutexGuard lock(mGuard);

		mAttachments.clear();
	}

	template<typename T>
	inline std::set<std::shared_ptr<T>> Attachment<T>::items()
	{
		removeInvalid();

		MutexGuard locker(mGuard);

		std::shared_ptr<T> ptr;
		std::set<std::shared_ptr<T>> ptrs;

		for (const auto &attachment : mAttachments)
		{
			if (ptr = attachment.lock())
			{
				ptrs.insert(ptr);
			}
		}

		return ptrs;
	}

}