#pragma once

#include <GRPrereq.h>
#include <set>

#include "mantle.h"
#include "GRMemoryRefSet.h"

namespace GREngine
{

class CommandBuffer
{
public:
	static CommandBufferSPtr getFromPool(const GR_FLAGS buildFlags = 0, const GR_QUEUE_TYPE queueType = GR_QUEUE_UNIVERSAL);
	static void returnToPool(CommandBufferSPtr cmdBuff);

	explicit CommandBuffer(const GR_CMD_BUFFER_CREATE_INFO &info);
	~CommandBuffer();

	GR_CMD_BUFFER grCmdBuffer() const;

	void reset();
	void begin(const GR_FLAGS buildFlags = 0);
	void end();

	void queue(const MemoryRefSet &memRefs);
	void queueBlocking(const MemoryRefSet &memRefs);

private:
	mutable std::mutex mGuard;

	GR_CMD_BUFFER mCommandBuffer;

	bool mIsInRecState;
};

}