#pragma once

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#include "GRPrereq.h"
#include "GRSceneObjectComponent.h"

namespace GREngine
{

	namespace SceneObjectComponents
	{

		class Transform : public SceneObjectComponent
		{
		public:
			explicit Transform(SceneObject *obj);
			~Transform();

			void move(DirectX::XMFLOAT3 vec);
			void rotate(DirectX::XMVECTOR quat);
			void rotateByAngles(DirectX::XMFLOAT3 angles);
			void rotateByRadians(DirectX::XMFLOAT3 rads);
			void scale(DirectX::XMFLOAT3 ratio);

			void moveTo(DirectX::XMFLOAT3 vec);
			void rotateToAngles(DirectX::XMFLOAT3 angles);
			void rotateToRadians(DirectX::XMFLOAT3 rads);
			void rotateTo(DirectX::XMVECTOR quat);
			void scaleTo(DirectX::XMFLOAT3 ratio);

			DirectX::XMFLOAT3 position() const;
			DirectX::XMVECTOR rotation() const;
			DirectX::XMFLOAT3 scale() const;

			DirectX::XMMATRIX matrix() const;

		private:
			mutable std::mutex mGuard;

			DirectX::XMVECTOR mPosition;
			DirectX::XMVECTOR mRotation;
			DirectX::XMVECTOR mScale;
		};

	}
}