#pragma once

#include "GRPrereq.h"
#include "GRViewport.h"

namespace GREngine
{
	namespace SceneObjectComponents
	{

		class Camera : public Viewport, public SceneObjectComponent
		{
		public:
			Camera(SceneObject *sceneObject);
			virtual ~Camera();

			virtual void update(int timeInMilis);

			virtual void setRenderTarget(const RenderTargetSPtr &colorTarget);

		private:
			TextureSPtr depthTexture;

			RenderTargetSPtr depthTarget;
		};

	}

}

