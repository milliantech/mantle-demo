#pragma once

#include <vector>

#include "GRPrereq.h"
#include "GRSingleton.h"

#include "mantle.h"

namespace GREngine
{

	class GPUMemoryManager : public Singleton<GPUMemoryManager>
	{
	public:
		GPUMemoryManager();
		~GPUMemoryManager();

		GPUMemorySPtr allocateObjectMemory(const GR_OBJECT &obj, const GR_ENUM priority = GR_MEMORY_PRIORITY_HIGH);

		GPUMemorySPtr allocateGPUMemory(const GR_GPU_SIZE size, const GR_FLAGS flags = 0, const GR_ENUM priority = GR_MEMORY_PRIORITY_NORMAL, const GR_GPU_SIZE alignment = 0);

		BufferSPtr allocateBufferFromGPUPool(const GR_GPU_SIZE size, const GR_FLAGS flags = 0, const GR_ENUM priority = GR_MEMORY_PRIORITY_NORMAL);

	private:
		void findSuitableHeaps(const GR_FLAGS flags, std::vector<GR_UINT> &heapIds) const;
		void createAllocInfo(std::vector<GR_UINT>& heapIds, GR_GPU_SIZE size, GR_FLAGS flags, GR_ENUM priority, GR_GPU_SIZE alignment,
			GR_MEMORY_ALLOC_INFO & allocInfo) const;

	private:
		std::mutex mPoolMutex;

		RenderSystem *mRenderSystem;

		std::vector<GR_MEMORY_HEAP_PROPERTIES> mHeapProps;

		std::vector<GPUMemorySPtr> mMemoryPool;
	};

}