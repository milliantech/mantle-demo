#pragma once

#include <vector>
#include <queue>
#include <mantle.h>
#include <mantleWsiWinExt.h>

#include "GRPrereq.h"
#include "GRMemoryRefSet.h"

namespace GREngine
{

	class Queue
	{
	public:
		explicit Queue(GR_ENUM type);
		~Queue();

		void queueCommand(GR_CMD_BUFFER command, const MemoryRefSet &releatedMemory, GR_FENCE fence = GR_NULL_HANDLE);
		void queueCommandBlocking(GR_CMD_BUFFER command, const MemoryRefSet &releatedMemory);

		void wsiPresent(GR_WSI_WIN_PRESENT_INFO *presentInfo);

		void flush();

		struct Entry
		{
			GR_CMD_BUFFER cmdBuff;
			std::vector<GR_MEMORY_REF> *memRefs;
			GR_FENCE fence;
			GR_WSI_WIN_PRESENT_INFO *wsiInfo;
		};

	private:

		void run();
		bool isQueueEmpty() const;

	private:
		mutable std::mutex mQueueMutex;
		std::mutex mFlushMutex;

		RenderSystem *mRenderSystem;

		GR_QUEUE mDeviceQueue;

		GR_CMD_BUFFER mFlushCommand;
		GR_FENCE mFlushFence;

		std::queue<Entry> mQueue;

		std::thread *mSubmitThread;

		std::atomic<bool> mActive;
	};

}