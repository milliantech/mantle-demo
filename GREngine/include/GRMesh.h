#pragma once

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#include "mantle.h"

#include "GRPrereq.h"

struct aiMesh;

namespace GREngine
{

	class Mesh
	{
	public:
		static MeshSPtr create(const std::string file);

	public:
		explicit Mesh(aiMesh *mMesh);
		virtual ~Mesh();

		GR_MEMORY_VIEW_ATTACH_INFO meshView(MemoryRefSet &refs) const;
		GR_MEMORY_VIEW_ATTACH_INFO indexiesView(MemoryRefSet &refs) const;

		GR_UINT indexiesCount() const;
		GR_UINT verticesCount() const;

	private:
		void initGpuMemory();
		void copyToGpu();

	private:
		BufferSPtr mMeshBuffer;
		BufferSPtr mIndexiesBuffer;

		aiMesh *mMesh;
	};

	struct RenderMeshVertex
	{
		DirectX::XMFLOAT4 pos;
		DirectX::XMFLOAT4 normal;
		DirectX::XMFLOAT2 uv;
		DirectX::XMFLOAT2 padding;
	};
}
