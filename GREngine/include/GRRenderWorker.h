#pragma once

#include <queue>

#include "GRPrereq.h"
#include "GRRenderTree.h"

namespace GREngine
{
	struct RenderPhaseParams
	{
		Scene *scene;
		ViewportSPtr viewport;
	};

	class GRExport RenderWorker
	{
	public:

		explicit RenderWorker();
		virtual ~RenderWorker();

		void renderScene(const RenderPhaseParamsSPtr &params, const SceneObjectSPtr &rootObj);

		void presentResult();

		bool isRenderRunning() const;
	private:

		void initRenderThreads(size_t count);
		void stopRenderThreads();

		void beginTraverse();
		void traverseScene(const RenderContext &context, const SceneObjectSPtr &sceneObject);
		void endTraverse();
		void beginRender();
		void endRender();

		void joinRender();

		void fetchNode(RenderNodeSPtr &node);
		void endNode();
		bool isCurrentAtEnd() const;

	private:
		friend class RenderThread;

		std::vector<RenderThreadSPtr> mThreads;

		RenderPhaseParamsSPtr mPhaseParams;

		RenderTreeSPtr mTree;
		RenderNodeIterator mCurrentNode;

		mutable std::mutex mTreeMutex;
		mutable std::mutex mTreeCondMutex;
		std::condition_variable mTreeCondition;

		mutable std::mutex mJoinConditionMutex;
		std::condition_variable mJoinCondition;

		std::atomic<bool> mActive;
	};
}

