#pragma once

#include <map>

#include "GRPrereq.h"
#include "GRSingleton.h"

namespace GREngine
{
	class RenderSystem;

	class GRExport SceneManager : public Singleton<SceneManager>
	{
	public:
		SceneManager();
		~SceneManager();

		SceneWPtr createScene(const std::string &id);
		SceneWPtr getScene(const std::string &id) const;
		void removeScene(SceneWPtr scene);

	private:
		void init();
		void shutdown();

	private:
		mutable std::mutex mGuard;

		RenderSystem *mRenderSystem;

		std::map<std::string, SceneSPtr> mScenes;
	};

}

