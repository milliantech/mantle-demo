#include "..\include\ChangeMaterialComponent.h"

#include <Material/GRSimpleMaterial.h>
#include <GRSceneObject.h>

namespace MantleGnomes
{
	using namespace GREngine;
	using namespace GREngine::SceneObjectComponents;
	using namespace GREngine::Materials;

	std::random_device ChangeMaterialComponent::sRandomDevice;
	std::mt19937 ChangeMaterialComponent::sRandomGenerator = std::mt19937(ChangeMaterialComponent::sRandomDevice());

	ChangeMaterialComponent::ChangeMaterialComponent(SceneObject *obj, SimpleMaterialSPtr material, int switchTime) :
		SceneObjectComponent(obj),
		mMaterial(material),
		mSwitchTime(switchTime),
		mTime(0)
	{
	}

	ChangeMaterialComponent::~ChangeMaterialComponent()
	{
	}

	void ChangeMaterialComponent::update(unsigned int timeInMilis)
	{
		mTime += timeInMilis;

		if (mTime > mSwitchTime)
		{
			std::uniform_real_distribution<float> colorDistr(0.0f, 1.0f);

			mMaterial->setColor({ colorDistr(sRandomGenerator), colorDistr(sRandomGenerator), colorDistr(sRandomGenerator) });

			mTime = 0;
		}
	}

}