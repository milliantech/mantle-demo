#include "Camera.h"

#include <GRSceneObject.h>
#include <GRInputManager.h>

#include <iostream>

namespace MantleGnomes
{

using namespace GREngine;
using namespace GREngine::SceneObjectComponents;

Camera::Camera(SceneObject *sceneObject) :
	SceneObjectComponent(sceneObject)
{
	mTransform = sceneObject->component<Transform>();

	InputManager &inputMgr = InputManager::getInstance();
	inputMgr.mousePosition(&mMouseX, &mMouseY);
}

Camera::~Camera()
{
}

void Camera::update(unsigned int timeInMilis)
{
	InputManager &inputMgr = InputManager::getInstance();

	DirectX::XMFLOAT3 move = DirectX::XMFLOAT3(0, 0, 0);
	DirectX::XMFLOAT3 angles = DirectX::XMFLOAT3(0,0,0);

	if (inputMgr.keyState(SDL_SCANCODE_LEFT))
		move.x -= moveSpeed * timeInMilis;
	else if (inputMgr.keyState(SDL_SCANCODE_RIGHT))
		move.x += moveSpeed * timeInMilis;
	else if (inputMgr.keyState(SDL_SCANCODE_UP))
		move.z += moveSpeed * timeInMilis;
	else if (inputMgr.keyState(SDL_SCANCODE_DOWN))
		move.z -= moveSpeed * timeInMilis;
	else if(inputMgr.keyState(SDL_SCANCODE_PAGEUP))
		move.y += moveSpeed * timeInMilis;
	else if(inputMgr.keyState(SDL_SCANCODE_PAGEDOWN))
		move.y -= moveSpeed * timeInMilis;
/*
	int mouseX = 0;
	int mouseY = 0;

	inputMgr.mousePosition(&mouseX, &mouseY);

	angles.y = rotationSpeed * timeInMilis * (mMouseX - mouseX);
	angles.x = rotationSpeed * timeInMilis * (mMouseY - mouseY);
*/
	mTransform->move(move);
	//mTransform->rotateToAngles(angles);
}

}