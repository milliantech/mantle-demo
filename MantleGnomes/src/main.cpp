#include <iostream>
#include <memory>
#include <GRKernel.h>

#include "GnomeApp.h"

int main(int argc, char* argv[])
{
	MantleGnomes::GnomeApp gnomeApp;

	GREngine::Kernel kernel(&gnomeApp);

	try
	{
		kernel.run();
	}
	catch (const std::exception &e)
	{
		std::cerr << "Critical error: " << e.what() << std::endl;
	}
	
	return 0;
};