#pragma once

#include <random>

#include <GRSceneObjectComponent.h>
#include <SceneObjectComponent/GRTransform.h>

namespace MantleGnomes
{

	class ChangeMaterialComponent : public GREngine::SceneObjectComponent
	{
	public:
		ChangeMaterialComponent(GREngine::SceneObject *obj, GREngine::Materials::SimpleMaterialSPtr material, int switchTime);
		~ChangeMaterialComponent();

		virtual void update(unsigned int timeInMilis) override;

	private:
		GREngine::Materials::SimpleMaterialSPtr mMaterial;

		unsigned int mTime;
		unsigned int mSwitchTime;

		static std::random_device sRandomDevice;
		static std::mt19937 sRandomGenerator;
	};

}